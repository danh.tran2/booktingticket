import React, { Component, Fragment } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import { HomeTemplate } from "./templates/HomeTemplate/HomeTemplate";
import { AdminTemplate } from "./templates/AdminTemplate/AdminTemplate";
import Home from "./pages/Home/Home";
import Login from "./pages/Login/Login";
import Admin from "./pages/Admin/Admin";
import Register from "./pages/Register";
import MovieDetail from "./pages/MovieDetail";
// chuyue63n home va login ve folder pages

export default class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Fragment>
          <Switch>
            <HomeTemplate exact path="/login" component={Login} />
            <HomeTemplate exact path="/" component={Home} />
            <HomeTemplate exact path="/home" component={Home} />
            <HomeTemplate exact path="/register" component={Register} />
            <Route
              exact
              path="/moviedetail/:maPhim"
              component={MovieDetail}
            />

            <AdminTemplate exact path="/admin" component={Admin} />
          </Switch>
        </Fragment>
      </BrowserRouter>
    );
  }
}
