import axios from "axios";
import {domain, groupID, userLogin} from "../config/settings"

export class QuanLyPhimService {
  layDanhSachPhim = () => {
    return axios({
      url: `${domain}/QuanLyPhim/LayDanhSachPhim?maNhom=${groupID}`,
      method: 'GET'
    });
  };

  layThongTinPhim = (maPhim) => {
    return axios({
      url:`${domain}/QuanLyRap/LayThongTinLichChieuPhim?maphim=${maPhim}`,
      method:'GET'
    })
  }
}

export const qlPhimService = new QuanLyPhimService();
