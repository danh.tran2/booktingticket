import axios from "axios";
import { domain, token, userLogin, groupID } from "../config/settings";

export class QuanLyNguoiDung {
  dangNhap = (userLogin) => {
    return axios({
      url: `${domain}/QuanLyNguoiDung/DangNhap`,
      method: "POST",
      data: userLogin,
    });
  };
}

export const qlNguoiDung = new QuanLyNguoiDung();
