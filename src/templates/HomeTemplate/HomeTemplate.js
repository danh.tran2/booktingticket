import React, { Fragment } from "react";
import { Route, NavLink } from "react-router-dom";
import { useSelector } from "react-redux";

// export const HomeTemplate = ({ Component, ...restParam }) => {
//   return (
//     <Route path={props.path {...props.exact}}
//       render={() => {
//         return <props.Component {...propsComponent} />;
//       }}
//     />
//   );
// };

const HomeLayout = (props) => {
  const taiKhoan = useSelector(
    (state) => state.quanLyNguoiDungReducer.taiKhoan
  );

  console.log("hookselect", taiKhoan);

  const renderLogin = () => {
    if (taiKhoan !== "") {
      return <span className="nav-link">Hello ! {taiKhoan}</span>;
    }
    return (
      <NavLink className="nav-link" to="/login">
        Login
      </NavLink>
    );
  };

  return (
    <Fragment>
      <nav className="navbar navbar-expand-sm navbar-light bg-light">
        <a className="navbar-brand" href="editlink">
          Navbar
        </a>
        <button
          className="navbar-toggler d-lg-none"
          type="button"
          data-toggle="collapse"
          data-target="#collapsibleNavId"
          aria-controls="collapsibleNavId"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon" />
        </button>
        <div className="collapse navbar-collapse" id="collapsibleNavId">
          <ul className="navbar-nav mr-auto mt-2 mt-lg-0">
            <li className="nav-item active">
              <NavLink className="nav-link" to="/home">
                Home
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/register">
                Register
              </NavLink>
            </li>
            <li className="nav-item">{renderLogin()}</li>
            <li className="nav-item dropdown">
              <a
                className="nav-link dropdown-toggle"
                href="editlink"
                id="dropdownId"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
              >
                Dropdown
              </a>
              <div className="dropdown-menu" aria-labelledby="dropdownId">
                <a className="dropdown-item" href="editlink">
                  Action 1
                </a>
                <a className="dropdown-item" href="editlink">
                  Action 2
                </a>
              </div>
            </li>
          </ul>
          <form className="form-inline my-2 my-lg-0">
            <input
              className="form-control mr-sm-2"
              type="text"
              placeholder="Search"
            />
            <button
              className="btn btn-outline-success my-2 my-sm-0"
              type="submit"
            >
              Search
            </button>
          </form>
        </div>
      </nav>
      {props.children}
    </Fragment>
  );
};
export const HomeTemplate = (props) => {
  return (
    <Route
      path={props.path}
      {...props.exact}
      // phải spread copy ra chứ ko lấy giá trị vì có nhiều props bool khác
      // override lại render của Route dưới dạng props, sau đó render được lifecycle gọi

      // Truyền thêm compon vô render
      render={(propsComponent) => {
        return (
          <HomeLayout>
            <props.component {...propsComponent} />
            {/* Viết tắt theo es6, gửi tag componet vô và kèm props thành child
            Child sau đó được gọi trong Layout chứ không được render ra
             spread props component ra và gửi lại hết thành props comp riêng vì propsCom có thể là 1 object */}
          </HomeLayout>
        );
      }}

      // Truyền không component ?
      // render={() => {
      //   return (
      //     <HomeLayout>
      //       <props.component/>
      //     </HomeLayout>
      //   );
      // }}
    />
  );
};
