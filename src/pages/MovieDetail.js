import React, { useState, useEffect } from "react";
import {NavLink} from "react-router-dom"

import { qlPhimService } from "../services/QuanLyPhimService";

export default function MovieDetail(props) {
  let [phim, setPhim] = useState({});

  useEffect(() => {
    qlPhimService.layThongTinPhim(props.match.params.maPhim).then((result) => {
      console.log(result.data);
      setPhim(result.data);
    });
  }, []);

  return (
    <div className="container">
      {/* {props.match.params.maPhim} */}
      <div className="row mt-3">
        <div className="col-4">
          <img src={phim.hinhAnh} alt={phim.tenPhim} />
        </div>
        <div className="col-8">
          <table className="table">
            <tr>
              <th style={{ width: 100 }}>Tên phim</th>
              <th>{phim.tenPhim}</th>
            </tr>
            <tr>
              <th>Mô tả</th>
              <th>{phim.moTa}</th>
            </tr>
          </table>
        </div>
      </div>
      <h3>Thông tin lịch chiếu</h3>
      <hr />
      <div className="row">
        <div
          className="nav flex-column nav-pills col-4"
          id="v-pills-tab"
          role="tablist"
          aria-orientation="vertical"
        >
          {phim.heThongRapChieu?.map((heThongRap, index) => {
            return (
              <a
                key={index}
                className="nav-link"
                id="v-pills-home-tab"
                data-toggle="pill"
                href={`#${heThongRap.maHeThongRap}`}
                role="tab"
                aria-controls="v-pills-home"
                aria-selected="true"
              >
                <img
                  src={heThongRap.logo}
                  style={{ width: 30, height: 30 }}
                  alt={heThongRap.logo}
                />
                {heThongRap.tenHeThongRap}
              </a>
            );
          })}
        </div>

        <div className="tab-content col-8" id="v-pills-tabContent">
          {phim.heThongRapChieu?.map((heThongRap, index) => {
            return (
              <div
                className="tab-pane fade show"
                id={heThongRap.maHeThongRap}
                role="tabpanel"
                aria-labelledby="v-pills-home-tab"
                key={index}
              >
                {heThongRap.cumRapChieu?.map((cumRap, index) => {
                  return (
                    <div key={cumRap.maCumRap}>
                      <h3>{cumRap.tenCumRap}</h3>
                      <div className="row">
                      {cumRap.lichChieuPhim?.slice(0,12).map((lichChieu,index)=>{
                          return <NavLink to='/' key={lichChieu.maLichChieu}>{lichChieu.ngayChieuGioChieu}</NavLink>
                      })}
                      </div>
                    </div>
                  );
                })}
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
}
