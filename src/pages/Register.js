import React, { Component } from "react";

export default class Register extends Component {
  state = {
    values: {
      hoTen: "",
      taiKhoan: "",
      matKhau: "",
      email: "",
      soDienThoai: "",
    },
    errors: {
      hoTen: "",
      taiKhoan: "",
      matKhau: "",
      email: "",
      soDienThoai: "",
    },
  };

  handleChangeInput = (event) => {
    // console.log(event);
    // var tagInput = event.target;
    let { value, name } = event.target;
    // Tạo ra object this.state.values mới
    let newValues = {
      ...this.state.values,
      [name]: value,
    };

    let newErrors = {
      ...this.state.errors,
      [name]: value === "" ? "Không được bỏ trống!" : "",
    };

    // Xét email
    if (name === "email") {
      let regexEmail = "[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,}$";
      if (value.match(regexEmail)) {
        newErrors.email = "";
      } else {
        newErrors.email = "Email không hợp lệ !";
      }
    }

    // Xét trường hợp rỗng
    // tạo ra object this.state.errors mới

    // setState lại values và errors
    this.setState({ values: newValues, errors: newErrors });
    // console.log(this.state);
  };

//   add hangdle submit ở form để check người dùng enter
  handleSubmit = (event) => {
    //   Chặn sự kiện load lại trang từ form submit
    event.preventDefault();
    let valid = true;
    let {values, errors} = this.state;
    for (let key in values){
        if(values[key] === ''){ //nếu có 1 TH rỗng thì ko hợp lệ
            valid = false;
        }
    }
    // check errors
    for (let key in errors) {
        if (errors[key] !== ''){
            valid = false;
        }
    }
    if(!valid){
        alert('Thông tin không hợp lệ!')
        return;
    }
    // Gọi api hoặc dispatch redux
  }

  render() {
    return (
      <div>
        <form className="container" onSubmit={this.handleSubmit}>
          <h3 className="dislay-4">Đăng kí</h3>
          <div className="form-group">
            <span>Họ tên</span>
            <input
              className="form-control"
              name="hoTen"
              onChange={this.handleChangeInput}
            />
            <span className="text-danger">{this.state.errors.hoTen}</span>
          </div>
          <div className="form-group">
            <span>Tài khoản</span>
            <input
              className="form-control"
              name="taiKhoan"
              onChange={this.handleChangeInput}
            />
            <span className="text-danger">{this.state.errors.taiKhoan}</span>
          </div>
          <div className="form-group">
            <span>Mật khẩu</span>
            <input
              className="form-control"
              name="matKhau"
              type="password"
              onChange={this.handleChangeInput}
            />
            <span className="text-danger">{this.state.errors.matKhau}</span>
          </div>
          <div className="form-group">
            <span>Email</span>
            <input
              className="form-control"
              name="email"
              onChange={this.handleChangeInput}
            />
            <span className="text-danger">{this.state.errors.email}</span>
          </div>
          <div className="form-group">
            <span>Số điện thoại</span>
            <input
              className="form-control"
              name="soDienThoai"
              onChange={this.handleChangeInput}
            />
            <span className="text-danger">{this.state.errors.soDienThoai}</span>
          </div>
          <div className="form-group">
            <button
              className="btn btn-success"
              type="submit"
              onChange={this.handleChangeInput}
            >
              Đăng kí
            </button>
          </div>
        </form>
      </div>
    );
  }
}
