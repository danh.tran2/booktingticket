import React, { useState } from "react";
import { qlNguoiDung } from "../../services/QuanLyNguoiDung";
//UseState là thay cho this.state tương ứng rcc
import { token, userLogin } from "../../config/settings";
import { useDispatch } from "react-redux";
import { dangNhapAction } from "../../redux/action/quanLyNguoiDungAction";

const Login = (props) => {
  const dispatch = useDispatch();

  let [state, setState] = useState({
    values: {
      taiKhoan: "",
      matKhau: "",
    },
    errors: {
      taiKhoan: "",
      matKhau: "",
    },
  });

  //   Thực tế viết như kiểu dưới
  let [result, setResult] = useState("Chưa submit");

  const handleChangeInput = (event) => {
    // console.log(event);
    // var tagInput = event.target;
    let { value, name } = event.target;
    // Tạo ra object this.state.values mới
    let newValues = {
      ...state.values,
      [name]: value,
    };

    let newErrors = {
      ...state.errors,
      [name]: value === "" ? "Không được bỏ trống!" : "",
    };

    setState({ values: newValues, errors: newErrors });
  };
  console.log(state);

  const handleSubmit = (event) => {
    event.preventDefault();
    // Chặn load lại trang
    qlNguoiDung
      .dangNhap(state.values)
      .then((res) => {
        localStorage.setItem(userLogin, JSON.stringify(res.data));
        localStorage.setItem(token, res.data.accessToken);
        dispatch(dangNhapAction(res.data.taiKhoan));
        props.history.push("/home");
      })
      .catch((error) => {
        console.log(error.response.data);
      });

    // setResult("Đăng nhập thành công!");
  };
  return (
    <form onSubmit={handleSubmit} className="container">
      <h3>{result}</h3>
      <h3 className="dislay-4">Đăng nhập</h3>
      <div className="form-group">
        <span>Tài khoản</span>
        <input
          name="taiKhoan"
          className="form-control"
          onChange={handleChangeInput}
        />
        <span className="text-danger">{state.errors.taiKhoan}</span>
      </div>
      <div className="form-group">
        <span>Mật khẩu</span>
        <input
          name="matKhau"
          type="password"
          className="form-control"
          onChange={handleChangeInput}
        />
        <span className="text-danger">{state.errors.matKhau}</span>
      </div>
      <div className="form-group">
        <button className="btn btn-success">Đăng nhập</button>
      </div>
    </form>
  );
};

export default Login;
